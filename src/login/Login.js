import './estilos.css';

function Login() {
  return (
    <div className="App">
        <div class="row">
            <div class="col">
                <nav class="navbar bg-body-tertiary">
                    <div class="container-fluid">
                        <a class="navbar-brand" href="#">
                            <img src="./images/logo.png" alt="Logo" width="60" height="60" class="topthing" />
                            La tienda de Dirka
                        </a>
                        <div class="row">
                            <div class="col">
                                <div class="Registrars1"></div>
                                <a href="/indexregister.html">
                                        <button type="button" class="Registrarse">Registrarse</button>
                                </a>
                            </div>
                            <div class="col">
                                <button type="button" class="ingresar">Ingresar</button>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="container-img">
                        <img src="./images/logo.png" alt="logo" />
                    </div>
                </div>
                <div class="col">
                    <div class="container-form">
                        <div class="formulario">
                            <h1>Inicio de sesión</h1>
                            <form method="post" action="./indexinicio.html">
                                <div class="username">Correo electronico
                                    <input type="email" placeholder="Correo electronico" required />
                                </div>
                                <div class="username">Contraseña
                                    <input type="password" placeholder="Contraseña" required />
                                </div>
                                <div class="recordar">¿Olvidó su contraseña?
                                </div>
                                <input type="submit" value="ingresar" />
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
  );
}

export default Login;
